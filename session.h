#ifndef SESSION
#define SESSION
#include "worker.h"
#include <QtCore>
#include <vector>
using namespace std;

class Session : public QObject {
    Q_OBJECT

public:
    Session(vector<int> &p_ths, string &p_file_type, vector<int> &p_beg, vector<int> &p_end, vector<string> &p_infiles,
            vector<string> &p_outfiles, vector<string> &p_col, vector<double> &p_shifts, vector<wstring> &p_markers, vector<string> &p_regions,
            unsigned int &p_ncores, string &p_prog_type, int &p_rows, bool &p_aut, int &p_time);
    ~Session();
public slots:
    void build();
    void Passing0(string &s);
    //void Passing1();
    //void toMain6(vector<string> &p_s);

private:
    void addThread(int th_type);
    void stopThreads();
    vector<int> ths;
    string file_type;
    vector<int> beg;
    vector<int> end;
    vector<string> infiles;
    vector<string> outfiles;
    vector<string> col;
    vector<double> shifts;
    vector<wstring> markers;
    unsigned int ncores;
    vector<string> regions;
    string prog_type;
    int rows;
    bool aut;
    int time;

signals:
    void stopAll();
    void fromSession0(string &s);
    //void fromSession1();
    //void fromSession6(vector<string> &p_s);
};

#endif // SESSION

