#ifndef WORKER
#define WORKER
#include <QtCore>
#include <string>
using namespace std;

class Worker : public QObject {
    Q_OBJECT

private:
    QObject *th; 	//класс, помещаемый в поток
    int th_type;	//тип потока
    string AnyToStr(int &i);

public:

    Worker(int &p_type, string &p_file_type, vector<int> &p_beg, vector<int> &p_end, vector<string> &p_infiles,
           vector<string> &p_outfiles, vector<string> &p_col, vector<double> &p_shifts, vector<wstring> &p_markers,
           vector<string> &p_regions, unsigned int &p_ncores, string &p_prog_type, int &p_rows, bool &p_aut, int &time);
    string file_type;
    vector<int> beg;
    vector<int> end;
    vector<string> infiles;
    vector<string> outfiles;
    vector<string> col;
    vector<double> shifts;
    vector<wstring> markers;
    unsigned int ncores;
    vector<string> regions;
    string prog_type;
    int rows;
    bool aut;
    int time;
    ~Worker();    

public slots:
    void process(); 	//создает и запускает поток
    void stop();    	//останавливает поток

signals:
    void finished(); 	//сигнал о завершении работы потока
    void th0Finished(string &s);
    //void fromWorker6(vector<string> &arr);
};

#endif // WORKER

