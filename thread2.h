#ifndef THREAD2
#define THREAD2

#include <QtCore>
#include <vector>
#include <string>
using namespace std;

class Thread2 : public QObject
{
    Q_OBJECT
public:
    Thread2();
    int Quant(string &file_type, int &beg, int &end, string &f1, vector<string> &col, int &shift);
    ~Thread2();
};

#endif // THREAD2

