#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include <QtCore>
#include <vector>
#include <memory>
#include "session.h"
#include "session2.h"
using namespace std;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void Filter();
    void Quantity();
    void SelectFile();
    void printMessage(string &s);
    void quantAddition(int &q);
    void about();
private:
    int sum;
    vector<int> quant;
    QString fileName;
    vector<QSpinBox*> sbs_beg;
    vector<QSpinBox*> sbs_end;
    void extensionAndCopyFiles(string &strfile);
    vector<string> StrToVector(string &s, char &delimiter);
    vector<wstring> StrToVector(wstring &s, wchar_t &delimiter);
    vector<double> StrToVector(QString &s, char &delimiter);
    string AnyToStr(int &i);
    void createActions();
    void preOperations();
    void saveParameters(const char *filename);
    void loadParameters(const char *filename);
    string p_file_type;
    unsigned int ncores;
    vector<int> p_ths;
    shared_ptr<Session> session;
    shared_ptr<Session2> session2;
    vector<string> p_infiles;
    vector<string> p_outfiles;
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H

