#include "session.h"
#include "worker.h"
#include "mainwindow.h"
#include <iostream>
using namespace std;

Session::Session(vector<int> &p_ths, string &p_file_type, vector<int> &p_beg, vector<int> &p_end, vector<string> &p_infiles,
                 vector<string> &p_outfiles, vector<string> &p_col, vector<double> &p_shifts, vector<wstring> &p_markers,
                 vector<string> &p_regions, unsigned int &p_ncores, string &p_prog_type, int &p_rows, bool &p_aut, int &p_time)
{
    ths = p_ths;
    file_type = p_file_type;
    beg = p_beg;
    end = p_end;
    infiles = p_infiles;
    outfiles = p_outfiles;
    col = p_col;
    shifts = p_shifts;
    markers = p_markers;
    ncores = p_ncores;
    regions = p_regions;
    prog_type = p_prog_type;
    rows = p_rows;
    aut = p_aut;
    time = p_time;
    build();
}

void Session::addThread(int th_type)
{
    Worker* worker = new Worker(th_type, file_type, beg, end, infiles, outfiles, col, shifts, markers, regions, ncores, prog_type, rows, aut, time);
    QThread* thread = new QThread;
    worker->moveToThread(thread);
    connect(thread, SIGNAL(started()), worker, SLOT(process()));
    connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
    connect(this, SIGNAL(stopAll()), worker, SLOT(stop()));
    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
    qRegisterMetaType<string>("string&");
    connect(worker, SIGNAL(th0Finished(string&)), this, SLOT(Passing0(string&)));
    //connect(worker, SIGNAL(th1Finished()), this, SLOT(Passing1()));
    //connect(worker, SIGNAL(fromWorker6(vector<string>&)), this, SLOT(toMain6(vector<string>&)));
    return ;
}


void Session::stopThreads()
{
    emit stopAll();
}

void Session::build()
{
    stopThreads();
    for(int i = 0; i < (signed)ths.size(); ++i) {
        addThread(ths.at(i));
    }
    return ;
}

void Session::Passing0(string &s)
{
    emit fromSession0(s);
}

/*void Session::Passing1()
{
    emit fromSession1();
}*/

/*void Session::toMain6(vector<string> &p_s)
{
    emit fromSession6(p_s);
}*/

Session::~Session()
{
    stopThreads();
}
