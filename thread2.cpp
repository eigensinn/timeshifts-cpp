#include "thread2.h"
#include <iostream>
#include <memory>
#include <algorithm>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <string.h>
#include <sstream>
#include <set>
#include <codecvt>
#include <iterator>

#include "ExcelFormat.h"

using namespace ExcelFormat;

Thread2::Thread2()
{

}

int Thread2::Quant(string &file_type, int &beg, int &end, string &f1, vector<string> &col, int &shift)
{
    map<string,int> colmap = {{ "A", 0 },{ "B", 1 },{ "C", 2 },{ "D", 3 },{ "E", 4 },{ "F", 5 },
                               { "G", 6 },{ "H", 7 },{ "I", 8 },{ "J", 9 },{ "K", 10 },{ "L", 11 },
                               { "M", 11 },{ "N", 12 },{ "O", 13 },{ "P", 14 },{ "Q", 15 },{ "R", 16 },
                               { "S", 17 },{ "T", 18 },{ "U", 19 },{ "V", 20 },{ "W", 21 },{ "X", 22 },
                               { "Y", 23 },{ "Z", 24 }};
    shared_ptr<vector<double> > years(new vector<double>);
    string xls = "xls";
    string xlsx = "xlsx";
    //Читаем существующий файл
    BasicExcel rb(f1.c_str());
    BasicExcelWorksheet* sheet = rb.GetWorksheet(0);
    int rownum = sheet->GetTotalRows();

    if (file_type == xls)
    {

    } else if (file_type == xlsx){

    }
    int q = 0;
    for (int i = 0; i < rownum; i++)
    {
        BasicExcelCell* cell = sheet->Cell(i, colmap.at(col.at(0)));
        double d = cell->GetDouble();
        years.get()->push_back(d);
    }
    int l_years = years.get()->size();
    for (int i = 0; i < l_years; i++)
    {
        double f = years.get()->at(i);
        for (int j = beg+i+1; j < end; j++)
        {
            double s = years.get()->at(j);
            int diff = abs(s - f);
            if (diff == shift)
            {
                q += 1;
            }
        }
    }
    return q;
}

Thread2::~Thread2()
{

}

