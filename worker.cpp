#include "worker.h"
#include "thread.h"
#include <iostream>
#include <sstream>
using namespace std;

Worker:: Worker(int &p_type, string &p_file_type, vector<int> &p_beg, vector<int> &p_end, vector<string> &p_infiles,
                vector<string> &p_outfiles, vector<string> &p_col, vector<double> &p_shifts, vector<wstring> &p_markers,
                vector<string> &p_regions, unsigned int &p_ncores, string &p_prog_type, int &p_rows, bool &p_aut, int &p_time)
{
    th = NULL;
    th_type = p_type;
    file_type = p_file_type;
    beg = p_beg;
    end = p_end;
    infiles = p_infiles;
    outfiles = p_outfiles;
    col = p_col;
    shifts = p_shifts;
    markers = p_markers;
    ncores = p_ncores;
    regions = p_regions;
    prog_type = p_prog_type;
    rows = p_rows;
    aut = p_aut;
    time = p_time;
}

Worker::~ Worker ()
{
    if (th != NULL) {
        delete th;
    }
}

string Worker::AnyToStr(int &i)
{
    stringstream ss;
    ss << i;
    return ss.str();
}

void Worker::process()
{
    if (th_type >= 0 && th_type < 8)
    {
        int b, e;
        if (aut)
        {
            b = (rows*th_type/ncores);
            e = (rows*(th_type+1)/ncores);
        } else {
            b = beg.at(th_type);
            e = end.at(th_type);
        }
        emit th0Finished("Поток "+AnyToStr(th_type)+" запущен");
        th = new Thread(file_type, b, e, infiles.at(th_type), outfiles.at(th_type), col, shifts, markers, regions, prog_type, time);
        emit th0Finished("Поток "+AnyToStr(th_type)+" завершен");
    }
    emit finished();
    return ;
}

void Worker::stop()
{
    if(th != NULL) {
        //th->stop();
    }
    return ;
}
