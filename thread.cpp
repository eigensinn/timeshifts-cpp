#include "thread.h"
#include <iostream>
#include <memory>
#include <algorithm>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <string.h>
#include <sstream>
#include <set>
#include <codecvt>
#include <iterator>

#include "ExcelFormat.h"

using namespace ExcelFormat;

string Thread::AnyToStr(int &i)
{
    stringstream ss;
    ss << i;
    return ss.str();
}

vector<string> Thread::StrToVector(string &s, char &delimiter)
{
    istringstream iss(s);
    vector<string> pieces;
    string piece;
    while (getline(iss, piece, delimiter)) {
        if (!piece.empty())
            pieces.push_back(piece);
    }
    return pieces;
}

string Thread::newOutFile(string &f2, int &i)
{
    char delimiter = '.';
    vector<string> pieces = StrToVector(f2, delimiter);
    string f2type = pieces.back();
    string newf2 = f2.substr(0, f2.size()-f2type.size()-1)+"_"+AnyToStr(i)+"."+f2type;
    return newf2;
}

Thread::Thread(string &file_type, int &beg, int &end, string &f1, string &f2, vector<string> &col,
               vector<double> &shifts, vector<wstring> &markers, vector<string> &regions, string &prog_type, int &time)
{
    map<string,int> colmap = {{ "A", 0 },{ "B", 1 },{ "C", 2 },{ "D", 3 },{ "E", 4 },{ "F", 5 },
                               { "G", 6 },{ "H", 7 },{ "I", 8 },{ "J", 9 },{ "K", 10 },{ "L", 11 },
                               { "M", 11 },{ "N", 12 },{ "O", 13 },{ "P", 14 },{ "Q", 15 },{ "R", 16 },
                               { "S", 17 },{ "T", 18 },{ "U", 19 },{ "V", 20 },{ "W", 21 },{ "X", 22 },
                               { "Y", 23 },{ "Z", 24 }};
    int row_number = 0;
    shared_ptr<vector<double> > years(new vector<double>);
    shared_ptr<vector<const char*> > regs(new vector<const char*>);
    shared_ptr<vector<wstring> > types(new vector<wstring>);
    string xls = "xls";
    string xlsx = "xlsx";
    //Читаем существующий файл
    BasicExcel rb(f1.c_str());
    BasicExcelWorksheet* sheet = rb.GetWorksheet(0);

    //Создаем файл для записи
    BasicExcel wb;
    wb.New(1);
    BasicExcelWorksheet* sheet_w = wb.GetWorksheet(0);
    XLSFormatManager fmt_mgr(wb);
    CellFormat fmt_double(fmt_mgr);
    fmt_double.set_format_string(XLS_FORMAT_INTEGER);
    CellFormat fmt_string(fmt_mgr);
    fmt_string.set_format_string(XLS_FORMAT_TEXT);

    if (file_type == xls)
    {

    } else if (file_type == xlsx){

    }
    int rownum = sheet->GetTotalRows();
    //Инициализируем конвертер из string в wstring
    wstring_convert<codecvt_utf8_utf16<wchar_t>> converter;

    for(int row = 1; row < rownum; ++row)
    {
        BasicExcelCell* cell = sheet->Cell(row, colmap.at(col.at(0)));
        double d = cell->GetDouble();
        years.get()->push_back(d);

        BasicExcelCell* cell_reg = sheet->Cell(row, colmap.at(col.at(1)));
        const char* row_reg = cell_reg->GetString();
        regs.get()->push_back(row_reg);

        BasicExcelCell* cell_type = sheet->Cell(row, colmap.at(col.at(2)));
        wstring row_type;
        //Определяем тип ячейки
        int cellType = cell_type->Type();
        //Если в ячейке кириллица
        if (cellType == 4)
        {
            row_type = cell_type->GetWString();
            types.get()->push_back(row_type);
        //Если в ячейке латиница
        } else if (cellType == 3){
            string row_typec = cell_type->GetString();
            //Конвертируем маркеры из string в wstirng
            wstring row_type = converter.from_bytes(row_typec);
            types.get()->push_back(row_type);
        } else {
            wstring row_type = L"None";
            types.get()->push_back(row_type);
        }
    }
    int l_years = years.get()->size();
    reverse(years.get()->begin(), years.get()->end());
    reverse(regs.get()->begin(), regs.get()->end());
    reverse(types.get()->begin(), types.get()->end());
    if (prog_type == "shifts")
    {
        for (int i = beg; i < end; i++)
        {
            if (row_number > 65500)
            {
                string extraf2 = newOutFile(f2, i);
                wb.SaveAs(extraf2.c_str());
                row_number = 0;
            }
            cout << i << endl;
            const char* row_reg_i = regs.get()->at(i);
            wstring row_type_i = types.get()->at(i);
            for (int j = i; j < l_years; j++)
            {
                double abs_years = abs(years.get()->at(i) - years.get()->at(j));
                if (find(shifts.begin(), shifts.end(), abs_years) != shifts.end())
                {
                    const char* row_reg_j = regs.get()->at(j);
                    wstring row_type_j = types.get()->at(j);
                    vector<double> newd = {abs_years, years.get()->at(i), years.get()->at(j)};
                    if (row_reg_i != NULL && row_reg_j != NULL)
                    {
                        if(row_type_i != L"None" && row_type_j != L"None")
                        {
                            if (row_type_i == row_type_j && strcmp(row_reg_i, row_reg_j) == 0)
                            {
                                for (int l = 0; l < (signed)newd.size(); l++)
                                {
                                    BasicExcelCell* cell = sheet_w->Cell(row_number, l);
                                    if (newd.at(l) != NULL)
                                    {
                                        cell->SetDouble(newd.at(l));
                                    }
                                }
                                BasicExcelCell* cell3 = sheet_w->Cell(row_number, 3);
                                cell3->SetString(row_reg_i);
                                BasicExcelCell* cell4 = sheet_w->Cell(row_number, 4);
                                cell4->SetWString(row_type_i.c_str());
                                row_number += 1;
                            }
                        }
                    }
                }
            }
        }
        wb.SaveAs(f2.c_str());
    } else if (prog_type == "chains") {
        //Берем срез вектора дат для текущего диапазона строк, обрабатываемого одним ядром процессора
        shared_ptr<vector<double> > vyears(new vector<double>);
        //Получаем вектор сдвигов
        shared_ptr<vector<double> > shyears(new vector<double>);
        for (int i = beg; i < end-1; i++)
        {
            vyears.get()->push_back(years.get()->at(i));
        }
        //Создаем контейнер для совпадений формата: год[тип события, тип события...]
        shared_ptr<map<double,vector<wstring> > > matches(new map<double,vector<wstring> >);
        //Создаем контейнер для сдвигов
        shared_ptr<map<double,vector<double> > > matches2(new map<double,vector<double> >);
        //Создаем пустой вектор-заполнитель
        vector<wstring> nullvector;
        vector<double> nullvector2;
        //Заполняем контейнер сдвигов
        for(int row = 1; row < rownum; ++row)
        {
            BasicExcelCell* cell = sheet->Cell(row, colmap.at(col.at(3)));
            double d = cell->GetDouble();
            shyears.get()->push_back(d);
        }
        reverse(shyears.get()->begin(), shyears.get()->end());
        //Создаем множество из дат
        shared_ptr<set<double> > syears(new set<double>(vyears.get()->begin(),vyears.get()->end()));
        for (int a = 0; a < (signed)regions.size(); a++)
        {
            //Заполняем множество датами и пустыми векторами
            for (auto it = syears.get()->begin(); it != syears.get()->end(); ++it)
            {
                if(matches.get()->count(*it) == 0)
                {
                    matches.get()->insert(make_pair(*it,nullvector));
                    matches2.get()->insert(make_pair(*it,nullvector2));
                }
            }
            //Проходим по всем строкам таблицы
            for (int i = beg; i < end-1; i++)
            {
                for (int x = 0; x < time; x++)
                {
                    if (i+x < end-1)
                    {
                        //Получаем тип
                        wstring type = types.get()->at(i+x);
                        double shyear = shyears.get()->at(i+x);
                        //Проходим по всем маркерам
                        for (int j = 0; j < (signed)markers.size(); j++)
                        {
                            wstring marker_j = markers.at(j);
                            if(type == marker_j && strcmp(regs.get()->at(i+x), regions.at(a).c_str()) == 0)
                            {

                                //...Добавляем тип события в контейнер совпадений
                                if (marker_j.c_str() != NULL)
                                {
                                    matches.get()->at(years.get()->at(i)).push_back(marker_j);
                                    matches2.get()->at(years.get()->at(i)).push_back(shyear);
                                }
                            }
                        }
                    }
                }
            }
            int row_decimal = row_number;
            vector<double> kfirst;
            //Записываем матрицу совпадений в файл xls
            for (auto k = matches.get()->begin(); k != matches.get()->end(); ++k)
            {
                //Создаем множество из вектора с найденными событиями
                set<wstring> smatches(k->second.begin(),k->second.end());
                int siz = k->second.size();
                //if(siz > 0 && siz < (signed)types.get()->size())
                if(siz > 0 && siz < 80)
                {
                    if(smatches.size() == markers.size())
                    {
                        BasicExcelCell* cell_r = sheet_w->Cell(row_number, 0);
                        cell_r->SetString(regions.at(a).c_str());
                        BasicExcelCell* cell = sheet_w->Cell(row_number, 1);
                        cell->SetDouble(k->first);
                        kfirst.push_back(k->first);
                        for (int m = 0; m < siz; m++)
                        {
                            cout << siz << " " << row_number << " " << m+2 << endl;
                            BasicExcelCell* cell_n = sheet_w->Cell(row_number, m+2);
                            try
                            {
                                cell_n->SetWString(k->second.at(m).c_str());
                            }
                            catch (...)
                            {

                            }
                        }
                        row_number += 2;
                    }
                }
            }
            matches.get()->clear();
            row_number = row_decimal + 1;
            for (auto p = matches2.get()->begin(); p != matches2.get()->end(); ++p)
            {
                for (int r = 0; r < (signed)kfirst.size(); r++)
                {
                    if(p->first == kfirst.at(r))
                    {
                        int siz = p->second.size();
                            for (int q = 0; q < siz; q++)
                            {
                                cout << "!!!" << siz << " " << row_number << " " << q+2 << endl;
                                BasicExcelCell* cell_n = sheet_w->Cell(row_number, q+2);
                                try
                                {
                                    cell_n->SetDouble(p->second.at(q));
                                }
                                catch (...)
                                {

                                }
                            }
                            row_number += 2;
                    }
                }
            }
            matches2.get()->clear();
            wb.SaveAs(f2.c_str());
        }
    }
}
Thread::~Thread()
{

}
