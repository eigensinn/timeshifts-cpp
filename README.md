# TimeShifts-cpp

## Приложение для поиска временных исторических сдвигов и цепочек подобных событий

  * Основано на библиотеке Qt;
  * Для сборки используется QtCreator с компилятором msvc.
  
## Руководство пользователя

[Скачать](https://bitbucket.org/eigensinn/timeshifts-cpp/src/master/manual/TimeShifts_v1.1.1.pdf)

## Получение необходимых библиотек после сборки с помощью windeployqt

Пример для msvc2015 64bit:

    C:\Users\user>cd C:\Qt\Qt5.10.1\5.10.1\msvc2015_64\bin
    C:\Qt\Qt5.10.1\5.10.1\msvc2015_64\bin>windeployqt.exe D:\Programming\workspace1\build-timeshifts-cpp-Desktop_Qt_5_10_1_MSVC2015_64bit-Release\release\timeshifts-cpp.exe

[Скачать timeshifts-cpp.exe](https://drive.google.com/open?id=0B9Bap8htsA28TVRQeEtUWHlfRTg)

## Лицензия

Приложение распространяется по лицензии [GPLV3](https://bitbucket.org/eigensinn/timeshifts-cpp/src/master/LICENSE.md)

2015, 2017, 2019 (c) Al Drondin

