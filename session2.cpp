#include "session2.h"
#include "worker2.h"
#include "mainwindow.h"
#include <iostream>
using namespace std;

Session2::Session2(vector<int> &p_ths, string &p_file_type, vector<int> &p_beg, vector<int> &p_end, vector<string> &p_infiles,
                 vector<string> &p_outfiles, vector<string> &p_col, unsigned int &p_ncores, int &p_rows, bool &p_aut, int &p_shift)
{
    ths = p_ths;
    file_type = p_file_type;
    beg = p_beg;
    end = p_end;
    infiles = p_infiles;
    outfiles = p_outfiles;
    col = p_col;
    ncores = p_ncores;
    rows = p_rows;
    aut = p_aut;
    shift = p_shift;
    build();
}

void Session2::addThread(int th_type)
{
    Worker2* worker = new Worker2(th_type, file_type, beg, end, infiles, outfiles, col, ncores, rows, aut, shift);
    QThread* thread = new QThread;
    worker->moveToThread(thread);
    connect(thread, SIGNAL(started()), worker, SLOT(process()));
    connect(worker, SIGNAL(finished()), thread, SLOT(quit()));
    connect(this, SIGNAL(stopAll()), worker, SLOT(stop()));
    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
    qRegisterMetaType<string>("string&");
    connect(worker, SIGNAL(th0Finished(string&)), this, SLOT(Passing0(string&)));
    qRegisterMetaType<int>("int&");
    connect(worker, SIGNAL(th1Finished(int&)), this, SLOT(Passing1(int&)));
    return ;
}


void Session2::stopThreads()
{
    emit stopAll();
}

void Session2::build()
{
    stopThreads();
    for(int i = 0; i < (signed)ths.size(); ++i) {
        addThread(ths.at(i));
    }
    return ;
}

void Session2::Passing0(string &s)
{
    emit fromSession0(s);
}

void Session2::Passing1(int &q)
{
    emit fromSession1(q);
}

Session2::~Session2()
{
    stopThreads();
}

