#ifndef WORKER2
#define WORKER2
#include <QtCore>
#include <string>
using namespace std;

class Worker2 : public QObject {
    Q_OBJECT

private:
    QObject *th; 	//класс, помещаемый в поток
    int th_type;	//тип потока
    string AnyToStr(int &i);

public:

    Worker2(int &p_type, string &p_file_type, vector<int> &p_beg, vector<int> &p_end, vector<string> &p_infiles,
           vector<string> &p_outfiles, vector<string> &p_col, unsigned int &p_ncores, int &p_rows, bool &p_aut, int &shift);
    string file_type;
    vector<int> beg;
    vector<int> end;
    vector<string> infiles;
    vector<string> outfiles;
    vector<string> col;
    unsigned int ncores;
    int rows;
    bool aut;
    int shift;
    ~Worker2();

public slots:
    void process(); 	//создает и запускает поток
    void stop();    	//останавливает поток

signals:
    void finished(); 	//сигнал о завершении работы потока
    void th0Finished(string &s);
    void th1Finished(int &q);
};

#endif // WORKER2

