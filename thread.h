#ifndef THREAD
#define THREAD

#include <QtCore>
#include <vector>
#include <string>
using namespace std;

class Thread : public QObject
{
    Q_OBJECT
public:
    Thread(string &file_type, int &beg, int &end, string &f1, string &f2, vector<string> &col, vector<double> &shifts,
           vector<wstring> &markers, vector<string> &regions, string &prog_type, int &time);
    ~Thread();
private:
    string newOutFile(string &f2, int &i);
    vector<string> StrToVector(string &s, char &delimiter);
    string AnyToStr(int &i);
};

#endif // THREAD

