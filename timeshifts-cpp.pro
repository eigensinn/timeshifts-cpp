#-------------------------------------------------
#
# Project created by QtCreator 2015-11-07T15:44:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = timeshifts-cpp

TEMPLATE = app


SOURCES += main.cpp \
    mainwindow.cpp \
    session.cpp \
    worker.cpp \
    thread.cpp \
    BasicExcel.cpp \
    ExcelFormat.cpp \
    session2.cpp \
    worker2.cpp \
    thread2.cpp

HEADERS += \
    mainwindow.h \
    session.h \
    worker.h \
    thread.h \
    BasicExcel.hpp \
    ExcelFormat.h \
    worker2.h \
    session2.h \
    thread2.h

FORMS    += timeshifts.ui

RESOURCES += application.qrc

CONFIG += c++11
