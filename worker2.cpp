#include "worker2.h"
#include "thread2.h"
#include <iostream>
#include <sstream>
using namespace std;

Worker2:: Worker2(int &p_type, string &p_file_type, vector<int> &p_beg, vector<int> &p_end, vector<string> &p_infiles,
                vector<string> &p_outfiles, vector<string> &p_col, unsigned int &p_ncores, int &p_rows, bool &p_aut, int &p_shift)
{
    th = NULL;
    th_type = p_type;
    file_type = p_file_type;
    beg = p_beg;
    end = p_end;
    infiles = p_infiles;
    outfiles = p_outfiles;
    col = p_col;
    ncores = p_ncores;
    rows = p_rows;
    aut = p_aut;
    shift = p_shift;
}

Worker2::~ Worker2 ()
{
    if (th != NULL) {
        delete th;
    }
}

string Worker2::AnyToStr(int &i)
{
    stringstream ss;
    ss << i;
    return ss.str();
}

void Worker2::process()
{
    if (th_type >= 0 && th_type < 8)
    {
        int b, e;
        if (aut)
        {
            b = (rows*th_type/ncores);
            e = (rows*(th_type+1)/ncores);
        } else {
            b = beg.at(th_type);
            e = end.at(th_type);
        }
        emit th0Finished("Поток "+AnyToStr(th_type)+" запущен");
        Thread2 *th2 = new Thread2();
        int q = th2->Quant(file_type, b, e, infiles.at(th_type), col, shift);
        emit th0Finished("Поток "+AnyToStr(th_type)+" завершен");
        emit th1Finished(q);
        delete th2;
    }
    emit finished();
    return ;
}

void Worker2::stop()
{
    if(th != NULL) {
        //th->stop();
    }
    return ;
}

