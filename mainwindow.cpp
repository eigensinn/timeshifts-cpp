#include <QtWidgets>
#include <QtGui>
#include <QtCore>
#include "mainwindow.h"
#include "ui_timeshifts.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include <iterator>
#include <thread>
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    createActions();
    preOperations();
    //loadParameters("C:\\workspace\\timeshifts-cpp\\param.dat");
    loadParameters("param.dat");
    sum = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    //saveParameters("C:\\workspace\\timeshifts-cpp\\param.dat");
    saveParameters("param.dat");
    QMainWindow::closeEvent(event);
}

void MainWindow::extensionAndCopyFiles(string &strfile)
{
    //Определение расширения
    char delimiter = '.';
    vector<string> pieces = StrToVector(strfile, delimiter);
    p_file_type = pieces.back();
    char delimiter2 = '/';
    vector<string> pieces2 = StrToVector(strfile, delimiter2);
    string p_file_name = pieces2.back();
    string sub_str = strfile.substr(0, strfile.size()-p_file_name.size()-1)+"/out_";

    //Создание копий исходного файла по количеству ядер процессора
    for (int i = 0; i < (signed)ncores; i++)
    {
        string new_strfile = strfile.substr(0, strfile.size()-p_file_type.size()-1)+"_"+AnyToStr(i)+"."+p_file_type;
        QFile::copy(strfile.c_str(), new_strfile.c_str());
        //Создание const char*-списка вновь созданных файлов
        p_infiles.push_back(new_strfile);
        //Создание списка выходных файлов
        string outfile = sub_str+AnyToStr(i)+"."+p_file_type;
        p_outfiles.push_back(outfile);
    }
}

void MainWindow::saveParameters(const char *filename)
{
    QString shifts = QString::number(ui->rb_shifts->isChecked());
    QString chains = QString::number(ui->rb_chains->isChecked());
    QString autointerval = QString::number(ui->cb_auto->isChecked());
    QVector<QString> pars = {ui->le_shifts->text(), ui->le_markers->text(), ui->le_col_year->text(), ui->le_col_reg->text(),
                             ui->le_col_type->text(), ui->le_regs->text(), ui->le_rows->text(), shifts, chains, autointerval,
                             ui->le_fileName->text(), ui->le_col_shift->text(), ui->sb_time->text(), ui->sb_shift->text(),
                             ui->le_col_size->text()};
    QFile file(filename);
    if(file.open(QIODevice::WriteOnly))
    {
        QDataStream stream(&file);
        stream.setVersion(QDataStream::Qt_5_5);
        stream << pars;
        if(stream.status() != QDataStream::Ok)
        {
            qDebug() << "Ошибка записи";
        }
    }
    file.close();
}

void MainWindow::loadParameters(const char *filename)
{
    QVector<QString> pars;
    QFile file(filename);
    if(file.open(QIODevice::ReadOnly))
    {
        QDataStream stream(&file);
        stream.setVersion(QDataStream::Qt_5_5);
        stream >> pars;
        if(stream.status() != QDataStream::Ok)
        {
            qDebug() << "Ошибка чтения файла";
        } else {
            ui->le_shifts->setText(pars.at(0));
            ui->le_markers->setText(pars.at(1));
            ui->le_col_year->setText(pars.at(2));
            ui->le_col_reg->setText(pars.at(3));
            ui->le_col_type->setText(pars.at(4));
            ui->le_regs->setText(pars.at(5));
            ui->le_rows->setText(pars.at(6));
            ui->rb_shifts->setChecked(pars.at(7).toInt());
            ui->rb_chains->setChecked(pars.at(8).toInt());
            ui->cb_auto->setChecked(pars.at(9).toInt());
            ui->le_fileName->setText(pars.at(10));
            ui->le_col_shift->setText(pars.at(11));
            ui->sb_time->setValue(pars.at(12).toInt());
            ui->sb_shift->setValue(pars.at(13).toInt());
            ui->le_col_size->setText(pars.at(14));
        }
    file.close();
    }
    //Пробуем при загрузке сделать копии файла, указанного в поле le_fileName
    fileName = ui->le_fileName->text();
    if (fileName != "")
    {
        try
        {
            extensionAndCopyFiles(fileName.toStdString());
        }
        catch (...)
        {
            SelectFile();
        }
    }
}

vector<string> MainWindow::StrToVector(string &s, char &delimiter)
{
    istringstream iss(s);
    vector<string> pieces;
    string piece;
    while (getline(iss, piece, delimiter)) {
        if (!piece.empty())
            pieces.push_back(piece);
    }
    return pieces;
}

vector<wstring> MainWindow::StrToVector(wstring &s, wchar_t &delimiter)
{
    wstringstream iss(s);
    vector<wstring> pieces;
    wstring piece;
    while (getline(iss, piece, delimiter)) {
        if (!piece.empty())
            pieces.push_back(piece);
    }
    return pieces;
}

vector<double> MainWindow::StrToVector(QString &s, char &delimiter)
{
    istringstream iss(s.toStdString());
    vector<double> pieces;
    std::string piece;
    while (getline(iss, piece, delimiter)) {
        if (!piece.empty())
            pieces.push_back(stod(piece));
    }
    return pieces;
}

string MainWindow::AnyToStr(int &i)
{
    stringstream ss;
    ss << i;
    return ss.str();
}

void MainWindow::preOperations()
{
    //Определяем количество ядер процессора
    ncores = std::thread::hardware_concurrency();
    for (int i = 0; i < (signed)ncores; i++)
    {
        p_ths.push_back(i);
        QSpinBox* sb_beg = new QSpinBox();
        sb_beg->setMaximum(65536);
        QSpinBox* sb_end = new QSpinBox();
        sb_end->setMaximum(65536);
        sbs_beg.push_back(sb_beg);
        sbs_end.push_back(sb_end);
        ui->gridLayout_8->addWidget(sb_beg,i+3,1);
        ui->gridLayout_8->addWidget(sb_end,i+3,2);
    }
    ui->le_ncores->setText(QString::number(ncores));
}

void MainWindow::Filter()
{
    vector<int> p_beg;
    vector<int> p_end;
    for (int i = 0; i < (signed)ncores; i++)
    {
        p_beg.push_back(sbs_beg.at(i)->text().toInt());
        p_end.push_back(sbs_end.at(i)->text().toInt());
    }
    vector<string> p_col = {ui->le_col_year->text().toStdString(), ui->le_col_reg->text().toStdString(), ui->le_col_type->text().toStdString(),
                           ui->le_col_shift->text().toStdString()};
    char delimiter = ',';
    char delimiter_reg = ':';
    wchar_t delimiter_mar = L',';
    QString s = ui->le_shifts->text();
    wstring m = ui->le_markers->text().toStdWString();
    string r = ui->le_regs->text().toStdString();
    vector<wstring> p_markers = StrToVector(m, delimiter_mar);
    vector<double> p_shifts = StrToVector(s, delimiter);
    vector<string> p_regions = StrToVector(r, delimiter_reg);
    string p_prog_type;
    if (ui->rb_shifts->isChecked())
    {
        p_prog_type = "shifts";
    } else if (ui->rb_chains->isChecked()) {
        p_prog_type = "chains";
    }
    int p_rows = ui->le_rows->text().toInt();
    bool p_aut = ui->cb_auto->isChecked();
    int p_time = ui->sb_time->value();
    if (m != L"" || (s != "" && r != ""))
    {
        session = shared_ptr<Session>(new Session(p_ths, p_file_type, p_beg, p_end, p_infiles, p_outfiles,
                                                  p_col, p_shifts, p_markers, p_regions, ncores, p_prog_type, p_rows, p_aut, p_time));
    }
    connect(session.get(), SIGNAL(fromSession0(string&)), this, SLOT(printMessage(string&)));
    p_ths.clear();
    p_ths.shrink_to_fit();
}

void MainWindow::Quantity()
{
    vector<int> p_beg;
    vector<int> p_end;
    for (int i = 0; i < (signed)ncores; i++)
    {
        p_beg.push_back(sbs_beg.at(i)->text().toInt());
        p_end.push_back(sbs_end.at(i)->text().toInt());
    }
    vector<string> p_col = {ui->le_col_size->text().toStdString()};
    int p_rows = ui->le_rows->text().toInt();
    bool p_aut = ui->cb_auto->isChecked();
    int p_shift = ui->sb_shift->value();
    if (p_shift != 0)
    {
        session2 = shared_ptr<Session2>(new Session2(p_ths, p_file_type, p_beg, p_end, p_infiles, p_outfiles, p_col, ncores, p_rows, p_aut, p_shift));
    }
    connect(session2.get(), SIGNAL(fromSession0(string&)), this, SLOT(printMessage(string&)));
    connect(session2.get(), SIGNAL(fromSession1(int&)), this, SLOT(quantAddition(int&)));
    //p_ths.clear();
    //p_ths.shrink_to_fit();
}

void MainWindow::printMessage(string &s)
{
    ui->te_messages->append(QString::fromStdString(s));
}

void MainWindow::quantAddition(int &q)
{
    if (quant.size() < ncores)
    {
        quant.push_back(q);
    }
    if (quant.size() == ncores)
    {
        for (int i = 0; i < (signed)ncores; i++)
        {
            sum += quant.at(i);
        }
        QString s = QString::number(sum);
        ui->te_messages->append(s);
        //quant.clear();
        //quant.shrink_to_fit();
        sum = 0;
    }
}

void MainWindow::SelectFile()
{
    //Выбор исходного файла
    fileName = QFileDialog::getOpenFileName(this, tr("Open Excel File"), QDir::currentPath(),
                                         "Excel Files (*.xls *.xlsx)");
    if (fileName != "") {
        string strfile = fileName.toStdString();
        ui->le_fileName->setText(fileName);
        extensionAndCopyFiles(strfile);
    }
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About TimeShifts"), tr("TimeShifts version 1.3.1\nAuthor: Alexey Drondin\nDate: December 2015"));
}

void MainWindow::createActions()
{
    connect(ui->action_about_TimeShifts, &QAction::triggered, this, &MainWindow::about);
    connect(ui->action_Qt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(ui->pb_filter, &QPushButton::clicked, this, &MainWindow::Filter);
    connect(ui->pb_quant, &QPushButton::clicked, this, &MainWindow::Quantity);
    connect(ui->pb_selectFile, &QPushButton::clicked, this, &MainWindow::SelectFile);
}
