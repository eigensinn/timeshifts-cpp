#ifndef SESSION2
#define SESSION2
#include "worker2.h"
#include <QtCore>
#include <vector>
using namespace std;

class Session2 : public QObject {
    Q_OBJECT

public:
    Session2(vector<int> &p_ths, string &p_file_type, vector<int> &p_beg, vector<int> &p_end, vector<string> &p_infiles,
            vector<string> &p_outfiles, vector<string> &p_col, unsigned int &p_ncores, int &p_rows, bool &p_aut, int &p_shift);
    ~Session2();
public slots:
    void build();
    void Passing0(string &s);
    void Passing1(int &q);

private:
    void addThread(int th_type);
    void stopThreads();
    vector<int> ths;
    string file_type;
    vector<int> beg;
    vector<int> end;
    vector<string> infiles;
    vector<string> outfiles;
    vector<string> col;
    unsigned int ncores;
    int rows;
    bool aut;
    int shift;

signals:
    void stopAll();
    void fromSession0(string &s);
    void fromSession1(int &q);
};

#endif // SESSION2

